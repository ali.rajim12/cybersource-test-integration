import {Payment} from './../models/payment';
import {Headers, Http} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import {UUID} from 'angular2-uuid';

@Injectable()
export class CheckoutService {
    // private baseUrl: string = 'https://n20ecomtest.nicasiabank.com/api'; // device-width
    private baseUrl: string = 'http://localhost:3000/api';
    private headers = new Headers({'Content-Type': 'application/json'});
    private payment: Payment;

    constructor(private http: Http) {
    }

    set setPaymentInformation(payment) {
        this.payment = payment;
    }

    get getPaymentInformation() {
        return this.payment;
    }

    initPayment(): Payment {
        let signedFieldNames = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,auth_trans_ref_no';
        let payment: Payment = {
            // access_key: '96c15918ad783ad2bd003caa948f2ffa', // test
            access_key: '', // test
            // access_key: '0a1dd70815053ef98f9b74673664778d', // Live
            // profile_id: '22236616-9E0D-401F-B802-9E351AE92147', // test
            profile_id: '',
            // profile_id: '2B1CCD46-08B8-4577-869C-E0D37432EB96', // Live
            signed_field_names: signedFieldNames,
            unsigned_field_names: 'card_type,card_number,card_expiry_date',
            locale: 'en',
            transaction_uuid: UUID.UUID(),
            signed_date_time: this.getUTCDate(), //"2017-02-28T14:38:33Z",
            transaction_type: 'sale',
            reference_number: Math.floor((Math.random() * 10000) + 1).toString(),
            amount: '100.00',
            currency: 'USD',
            payment_method: 'card',
            bill_to_forename: 'N/A',
            bill_to_surname: 'N/A',
            bill_to_email: 'ali.rajim12@gmail.com',
            bill_to_phone: '',
            bill_to_address_line1: 'N/A',
            bill_to_address_city: 'N/A',
            bill_to_address_state: 'N/A',
            bill_to_address_country: 'NP',
            bill_to_address_postal_code: 'N/A',
            card_type: '001',
            card_number: '',
            card_expiry_date: '',
            signature: '',
            auth_trans_ref_no: this.generateRandom(20)
        };

        return payment;
    }

    generateRandom(length: number) {
        let result = '';
        let characters = '0123456789';
        let charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    encrypt(form): Observable<any> {
        return this.http
            .post(`${this.baseUrl}/sign`, JSON.stringify(form), {headers: this.headers})
            .map(res => res.json())
            .catch(this.handleError);
    }

    private getUTCDate(): string {
        var now = new Date();
        var now_utc = now.getUTCFullYear() + '-' + this.appendZero(now.getUTCMonth() + 1) + '-' + this.appendZero(now.getUTCDate()) + 'T' + this.appendZero(now.getUTCHours()) + ':' + this.appendZero(now.getUTCMinutes()) + ':' + this.appendZero(now.getUTCSeconds()) + 'Z';
        return now_utc;
    }

    private getISOTime(): string {
        var now = new Date();
        return now.toUTCString();
    }

    private appendZero(digit): string {
        if (digit < 10) {
            return '0' + digit;
        } else {
            return digit;
        }
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get  a better message
        let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
