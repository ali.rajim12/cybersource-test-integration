const express = require('express');
const router = express.Router();
const crypto = require('crypto');
// const SECRET_KEY = '60d645144caf4cb9bff7d272e7e6ed335c7c5cf5d46b4c71b0d3f19eeebd8eb1f85fdb37ef2743c0b17eef11b9850671c929c3c4530442d1912415872a907018cd85b7cdeb0842cfa5869ddae16c90d7dfff332bdc664067a4c02f110e3534c5bdb433a6db6044d6af33d196fcf9fffcb110dd883fb24b889ca054addc96ae68';
// test
// const SECRET_KEY = '2c1c573dff79486e83a10eb5b582f9d4f5e0da8933694ea9906848947ec7759b10922e69ac424e368b8db14d62a712b59fdde187d71844a0b90f87afe02e95b82bf5abf816864be28f2402d178cf582625dd6c6beb9942e5889d527aa498c137e6e794051ed044aa9dad73bb0d0ae35e3506f9ca39384cd6ab054730ee84211f'
/* GET api listing. */
const SECRET_KEY = '';
router.post('/sign', (req, res) => {

    let signedFields = req.body.signed_field_names.split(",");
    var fieldValues = [];
    signedFields.forEach(item => {
        fieldValues.push(item + "=" + req.body[item]);
    });

    const hash = crypto.createHmac('sha256', SECRET_KEY)
        .update(fieldValues.join(","))
        .digest('base64');
    res.send({'hash': hash});
});

module.exports = router;
